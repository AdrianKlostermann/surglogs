/// <reference types="Cypress" />
context('SurgLogs - Narcotic logbook 02', () => {
    beforeEach(() => {
        cy.visit('https://surglogs.com/')
    });

  
    it('.type() - type into a DOM element', () => {
        
        // Clear cookies and local storage//
		cy.clearCookies();
		cy.clearLocalStorage();
		// Data mock //
		cy.server()
			.route("POST", '/api/v2/client/sites/528818/visit-data?sv=6')
				.as('Data');
		
		// Dashboard mock //
		cy.server()
			.route("GET", 'api/v11/clinics/1025/modules')
				.as('DashboardModules');
		
		// Signature mock //
		cy.server()
			.route("POST", '/api/v11/clinics/1025/files/signatures')
				.as('Signatures');
		
		// Login navigation //
		cy.get('#log-in')
			.click({force:true});
			
		cy.get('[data-cy="login-form"]')
			.contains('Sign in to SurgLogs');
		
		// Credentials //
		cy.get('[data-cy="login-form-email"]')
			.type('adrian.klostermann@gmail.com')
				.should('have.value','adrian.klostermann@gmail.com');
				
		cy.get('[data-cy="login-form-password"]')
			.type('aDio1596321!');

		// Logging in //
		cy.get('[data-cy="login-form-submit"]')
			.click()
				
        // Wait for data //	
		cy.wait('@Data',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);
		
		// Wait for dashboard to load//
        cy.wait('@DashboardModules',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);
		
		// URL verification //
		cy.url().should('eq', 'https://surglogs.com/dashboard');			
		
			
		// Navigation to Narcotic logbook module//		
		cy.get('[data-cy="dashboard-modules-list-item"]')
			.eq(1)
				.click();
				
		// Wait for data //	
		cy.wait('@Data',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);

		
		cy.get('[class="css-178ti2q e1pava8t0"]')
			.click();
				
		
		// Adding new medication //
		cy.get('input[name="medications[0].name"]')
			.type('Paralen');
			
		cy.get('input[name="medications[0].dosage"]')
			.type('100');
			
		cy.get('div[class="Select-control"]')
			.type('Tablet{enter}');
			
		// Saving medicament //
		cy.get('button[type="submit"]')
			.click();
		
			
		cy.wait(2000);
			
		// Navigation to Main Cabinet //
		cy.get('div[class="Select-value"]')
			.type('Main{enter}');
		
			
		// Record transaction button and replenish inventory //
		cy.get('div[class="css-1ew591x eru2ugn0"]')
			.click();
			
		cy.get('div[class="css-uv6h3v eru2ugn1"]')
			.click();
			
		// Wait for data //	
		cy.wait('@Data',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);

		
		// Adding medications //
		cy.get('div[class="Select-placeholder"]')
			.first()
				.type('Paralen{enter}');
			
		cy.get('div[class="css-1mwj805 e7gbt9h1"]')
			.type('100');
		
		
		// Adding signature //	
		cy.get('[data-cy="narclogs-signatures-add-signature-button"]')
			.first()
				.click();
			
		// Signature //
		cy.get('[data-cy="narclogs-signatures-add-signature-pad"]')
 			.trigger('mousedown', { which: 1 })
 			.trigger('mousemove', { clientX: 400, clientY: 500 })
  			.trigger('mouseup', {force: true})
		
		
		
		// Confirm signature//
		cy.get('[data-cy="narclogs-signatures-add-signature-pad-confirm-button"]')
			.click();
		
		// Wait for data //
		cy.wait('@Data',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);
			
		// Wait to load signature //		
		cy.wait('@Signatures',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);

		
		// Submit button //
		cy.get('button[type="submit"]')
			.click();
		
		// Warning confirmation //
		cy.get('a[class="css-178ti2q e1pava8t0"]')
			.click();
			
		
		
		
		// Dispense medication navigation //
		cy.get('div[class="css-1ew591x eru2ugn0"]')
			.click();
			
		cy.get('span[class="css-10ln2yy er2lof50"]')
			.eq(0)
				.click();
				
		// Wait for data //	
		cy.wait('@Data',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);

				
		// Adding patient information //
		cy.get('[data-cy="narclogs-dispense-patient-first-name"]')
			.type('Test');
			
		cy.get('[data-cy="narclogs-dispense-patient-last-name"]')
			.type('Patient');
			
		// Date set //
		cy.get('[data-cy="narclogs-dispense-patient-dob"]')
			.click();
			
		cy.get('[class="react-datepicker__day react-datepicker__day--004"]')
			.click();
		
		// Add patient button //
		cy.get('button[type="submit"]')
			.click();
		
		// Skip adding sticker //
		cy.get('a[class="css-18xarw9 e1pava8t0"]')
			.click();
		
		// Dispensing medication //
		cy.get('[data-cy="narclogs-transaction-select-medication-dropdown"]')
			.first()
				.type('Paralen{enter}')
			
		cy.get('input[placeholder="10"]')
			.type('5');
			
		// Dosage concentration verification //		
		cy.get('[class="css-o6t2j0 efv9aky1"]')
			.contains('100').should('be.visible');
				
		// Adding signature 1 //	
		cy.get('[data-cy="narclogs-signatures-add-signature-button"]')
			.first()
				.click();
			
		// Signature 1 //
		cy.get('[data-cy="narclogs-signatures-add-signature-pad"]')
 			.trigger('mousedown', { which: 1 })
 			.trigger('mousemove', { clientX: 400, clientY: 500 })
  			.trigger('mouseup', {force: true})
	
		// Confirm signature 1//
		cy.get('[data-cy="narclogs-signatures-add-signature-pad-confirm-button"]')
			.click();
			
		// Wait to load signature //		
		cy.wait('@Signatures',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200)
		
		// Selecting user for signature 2 //		
		cy.get('[data-cy="narclogs-signatures-select-user-dropdown"]')
			.eq(1)
				.type('Other{enter}')
		
		cy.get('input[name="signature2_other"]')
			.type('Test_signature');
			
		// Adding signature 2 //
		cy.get('[data-cy="narclogs-signatures-add-signature-button"]')
			.eq(1)
				.click();
			
		// Signature 2 //
		cy.get('[data-cy="narclogs-signatures-add-signature-pad"]')
 			.trigger('mousedown', { which: 1 })
 			.trigger('mousemove', { clientX: 400, clientY: 500 })
  			.trigger('mouseup', {force: true})
	
		// Confirm signature 2//
		cy.get('[data-cy="narclogs-signatures-add-signature-pad-confirm-button"]')
			.click();
			
		// Wait to load signature //
		cy.wait('@Signatures',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);
			
		// Submit Dispense //		
		cy.get('button[type="submit"]')
			.click();
			
		// Wait for data //	
		cy.wait('@Data',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);
		
		// Amount verification //
		cy.get('span[class="css-1ihs5w6 e18d3c6j0"]')
			.contains('95').should('be.visible');
			
		
		
		
		
		// Waste medication navigation //
		cy.get('div[class="css-1ew591x eru2ugn0"]')
			.click();
			
		cy.get('span[class="css-10ln2yy er2lof50"]')
			.eq(1)
				.click();
				
		// Choosing medication //		
		cy.get('[data-cy="narclogs-transaction-select-medication-dropdown"]')
			.first()
				.type('Paralen{enter}')
			
		cy.get('input[placeholder="10"]')
			.type('10');
			
		cy.get('div[class="css-19vkx4j eg95km40"]')
			.eq(1)
				.type('Refused{enter}');
			
		// Adding signature 1 //	
		cy.get('[data-cy="narclogs-signatures-add-signature-button"]')
			.first()
				.click();
			
		// Signature 1 //
		cy.get('[data-cy="narclogs-signatures-add-signature-pad"]')
 			.trigger('mousedown', { which: 1 })
 			.trigger('mousemove', { clientX: 400, clientY: 500 })
  			.trigger('mouseup', {force: true})
	
		// Confirm signature 1//
		cy.get('[data-cy="narclogs-signatures-add-signature-pad-confirm-button"]')
			.click();
			
		cy.wait('@Signatures',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);
		
		// Selecting user for signature 2 //		
		cy.get('[data-cy="narclogs-signatures-select-user-dropdown"]')
			.eq(1)
				.type('Other{enter}')
		
		cy.get('input[name="signature2_other"]')
			.type('Test_signature');
			
		// Adding signature 2 //
		cy.get('[data-cy="narclogs-signatures-add-signature-button"]')
			.eq(1)
				.click();
				
				
		// Signature 2 //
		cy.get('[data-cy="narclogs-signatures-add-signature-pad"]')
 			.trigger('mousedown', { which: 1 })
 			.trigger('mousemove', { clientX: 400, clientY: 500 })
  			.trigger('mouseup', {force: true})
	
		// Confirm signature 2//
		cy.get('[data-cy="narclogs-signatures-add-signature-pad-confirm-button"]')
			.click();
			
		// Wait to load signature //		
		cy.wait('@Signatures',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200)
		
		// Submit Waste //
		cy.get('button[type="submit"]')
			.click();
		
		// Wait for data //	
		cy.wait('@Data',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);

		
		// Amount verification //
		cy.get('span[class="css-1ihs5w6 e18d3c6j0"]')
			.contains('85').should('be.visible');
		
		
		
			
		// Correction navigation //
		cy.get('div[class="css-1ew591x eru2ugn0"]')
			.click();
			
		cy.get('span[class="css-10ln2yy er2lof50"]')
			.eq(3)
				.click();
				
		// Wait for data //	
		cy.wait('@Data',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);

				
				
		// Medication select //
		cy.get('div[class="css-19vkx4j eg95km40"]')
			.type('Paralen{enter}');
			
			
		// Current amount verification//		
		cy.get('span[class="css-1cuxlau ef0jqfv0"]')
			.contains(85).should('be.visible');
		
		// Correction type //
		cy.get('div[class="css-19vkx4j eg95km40"]')
			.eq(1)
			.type('Subtract{enter}');
		
		// Amount //
		cy.get('div[class="css-11schih e1i9gy1f1"]')
			.type('10');
			
		// Adding signature 1 //	
		cy.get('[data-cy="narclogs-signatures-add-signature-button"]')
			.first()
				.click();
			
		// Signature 1 //
		cy.get('[data-cy="narclogs-signatures-add-signature-pad"]')
 			.trigger('mousedown', { which: 1 })
 			.trigger('mousemove', { clientX: 400, clientY: 500 })
  			.trigger('mouseup', {force: true})
	
		// Confirm signature 1//
		cy.get('[data-cy="narclogs-signatures-add-signature-pad-confirm-button"]')
			.click();
			
		// Wait to load signature //		
		cy.wait('@Signatures',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);

		
		// Selecting user for signature 2 //		
		cy.get('[data-cy="narclogs-signatures-select-user-dropdown"]')
			.eq(1)
				.type('Other{enter}')
		
		cy.get('input[name="signature2_other"]')
			.type('Test_signature');
			
		// Adding signature 2 //
		cy.get('[data-cy="narclogs-signatures-add-signature-button"]')
			.eq(1)
				.click();
				
		
		// Signature 2 //
		cy.get('[data-cy="narclogs-signatures-add-signature-pad"]')
 			.trigger('mousedown', { which: 1 })
 			.trigger('mousemove', { clientX: 400, clientY: 500 })
  			.trigger('mouseup', {force: true})
	
		// Confirm signature 2//
		cy.get('[data-cy="narclogs-signatures-add-signature-pad-confirm-button"]')
			.click();
			
		// Wait to load signature //		
		cy.wait('@Signatures',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);
		
		// Submit Waste //
		cy.get('button[type="submit"]')
			.click();
			
			
		// Amount verification //
		cy.get('span[class="css-1ihs5w6 e18d3c6j0"]')
			.contains(75).should('be.visible');
			
		// Wait for data //	
		cy.wait('@Data',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200)
			
		// Comparing All medications //		
		cy.get('span[class="css-1ihs5w6 e18d3c6j0"]').invoke('text').then($Current_amount => {
    	cy.get('div[class="css-19vkx4j eg95km40"]').type('All{enter}')
		cy.get('span[class="css-1ihs5w6 e18d3c6j0"]')
			.first()
				.contains($Current_amount).should('be.visible')});
		
		// Clean up //
		cy.get('a[class="e1c2toxf0 css-pqanc1 e1pava8t0"]')
				.first()
					.click();
					
		// Wait for data //	
		cy.wait('@Data',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200)
		
		cy.get('a[class="e1owdiz01 css-tpxpbq e1pava8t0"]')
			.first()
				.click();
				
		cy.get('a[class="css-178ti2q e1pava8t0"]')
			.first()
				.click();
				
		// Clear cookies and local storage//
		cy.clearCookies();
		cy.clearLocalStorage();
			
    });
});

