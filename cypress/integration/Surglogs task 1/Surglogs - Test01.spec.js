/// <reference types="Cypress" />
context('SurgLogs - Surgical Logs 01', () => {
    beforeEach(() => {
        cy.visit('https://surglogs.com/')
    });

  
    it('.type() - type into a DOM element', () => {
        
		// Delete cookies and local storage //
		cy.clearCookies();
		cy.clearLocalStorage()
		
		// Login navigation //
		cy.get('#log-in')
			.click({force:true});
			
		cy.get('[data-cy="login-form"]')
			.contains('Sign in to SurgLogs');
		
		// Credentials //
		cy.get('[data-cy="login-form-email"]')
			.type('adrian.klostermann@gmail.com')
				.should('have.value','adrian.klostermann@gmail.com');
				
		cy.get('[data-cy="login-form-password"]')
			.type('aDio1596321!');

		// Data mock //
		cy.server()
			.route("POST", '/api/v2/client/sites/528818/visit-data?sv=6')
				.as('Data1');
		
		// Dashboard mock //
		cy.server()
			.route("GET", 'api/v11/clinics/1025/modules')
				.as('DashboardModules');
		
		
		// Logs mock //
		cy.server()
			.route("GET", '/api/v11/clinics/1025/logs')
				.as('Logs');
				
		// Logging in //
		cy.get('[data-cy="login-form-submit"]')
			.click()
			
		// Wait for data //	
		cy.wait('@Data1',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);
        
		// Wait for dashboard to load//
        cy.wait('@DashboardModules',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);
		
		// URL verification //
		cy.url().should('eq', 'https://surglogs.com/dashboard');
			
		// Navigation to Surgical logs module//		
		cy.get('[data-cy="dashboard-modules-list-item"]')
			.first()
				.click();
				
		
			
		cy.get('[class="css-5mgl16 eoik5g10"]')
			.contains('Surgical Logs').should('be.visible');
			
		cy.wait('@Data1',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);
			
		// Add new log button //		
		cy.get('[data-cy="log-list-parameters-add-button"]')
			.click();
		
		// Creating log and fields //
		cy.get('[data-cy="log-create-name"]')
			.type('Test_log');
		
		// Wait for data //	
		cy.wait('@Data1',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);
			
		cy.get('[data-cy="log-create-frequency"]')
			.click();
			
		cy.get('#react-select-2--option-1')
			.click();
			
		cy.get('[data-cy="log-create-param-name"]')
			.type('Temperature');
			
		cy.get('[class="Select-placeholder"]')
			.click();
			
		cy.get('#react-select-3--option-0')
			.click();
			
		cy.get('.Select-placeholder')
			.click();
			
		cy.get('#react-select-4--option-2')
			.click();
			
		// Add Date parameter //		
		cy.get('[data-cy="add-button"]')
			.click();
			
		cy.get('[name="param.params[1].name"]')
			.type('Date');
			
		cy.get('.Select-placeholder')
			.type('Date {enter}}');
		
		
		// Add single select parameter //		
		cy.get('[data-cy="add-button"]')
			.click();
		
		cy.get('[name="param.params[2].name"]')
			.type('Do you have kids?');
			
		cy.get('.Select-placeholder')
			.type('single {enter}');
		
		cy.get('input[name="param.params[2].choices[0].choice"]')
			.type('Yes');
		
		// Add another choice //
		cy.get('[class="css-i52un5 e1yuhfn93"]')
			.click();
			
		cy.get('input[name="param.params[2].choices[1].choice"]')
			.type('no');
			
		// Save new log //		
		cy.get('[data-cy="log-create-save-button"]')
			.click();
			
		cy.wait('@Data1',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);
			
		
		// Add entry for log //	
		cy.get('[class="eqeiof50 css-zcsmwv e1pava8t0"]')
			.click();
			
		// Wait for data //	
		cy.wait('@Data1',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);
			
		// Inserting temperature //
		cy.get('input[name="items[0].valueSingle"]')
			.type('50')
			
		// Inserting date //
		cy.get('input[name="items[1].valueSingle"]')
			.click();
			
		cy.get('[aria-label="day-5"]')
			.click();
		
		cy.get('input[name="items[2].valueSingle"]')
			.first()
				.click({force:true});
				
		
				
		// Submit button //			
		cy.get('button[type="submit"]')
			.click();
			
			
		// Wait for data //	
		cy.wait('@Data1',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);
			
		cy.wait('@Logs',{timeout:60000},{multiple:true})
			.should('have.property', 'status', 200);
			
		// Entry verification //		
		cy.get('span[class="css-8scb8d e1l7hfek0"]')
			.contains('50')
				.should('be.visible');
			
		cy.get('span[class="css-1ce6yu9 eo792tb0"]')
			.contains('02/05/20')
				.should('be.visible');
			
		cy.get('span[class="css-1ce6yu9 eo792tb0"]')
			.contains('Yes')
				.should('be.visible');
			
		// Clean up //		
		cy.get('a[class="eqeiof50 css-1ke0c30 e1pava8t0"]')
			.last()
				.click();
			
		cy.get('a[class="css-178ti2q e1pava8t0"]')
			.click();
			
		// Clear cookies and local storage//
		cy.clearCookies();
		cy.clearLocalStorage();
		
		
			
			
			
				
			
    });
});

